import Async from 'async';
import type { AsyncResultArrayCallback } from 'async';
import faker from 'faker';
import times from 'lodash.times';
import mysql from 'mysql';
import type { Connection } from 'mysql';
import config from './config';


interface User {
  name: string;
  family: string;
  age: number;
  roleID: number;
}

function createConnect(connection: Connection) {
  return function connect(callback: AsyncResultArrayCallback<undefined, Error>) {
    connection.connect((error) => {
      if (error !== null) {
        return callback(error);
      }
      console.info('> Database connection made.');
      return callback();
    });
  };
}

function createDropRoleTable(connection: Connection) {
  return function dropRoleTable(callback: AsyncResultArrayCallback<undefined, Error>) {
    connection.query('DROP TABLE IF EXISTS `Role`', (error) => {
      if (error !== null) {
        return callback(error);
      }
      console.info('> "Role" table dropped.');
      return callback();
    });
  };
}

function createDropUserTable(connection: Connection) {
  return function dropUserTable(callback: AsyncResultArrayCallback<undefined, Error>) {
    connection.query('DROP TABLE IF EXISTS `User`', (error) => {
      if (error !== null) {
        return callback(error);
      }
      console.info('> "User" table dropped.');
      return callback();
    });
  };
}

function createCreateRoleTable(connection: Connection) {
  return function createRoleTable(callback: AsyncResultArrayCallback<undefined, Error>) {
    connection.query(`
CREATE TABLE \`Role\` (
  \`ID\` INT NOT NULL AUTO_INCREMENT,
  \`RoleName\` VARCHAR(128) NOT NULL,
  PRIMARY KEY (\`ID\`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
`, (error) => {
      if (error !== null) {
        return callback(error);
      }
      console.info('> "Role" table created.');
      return callback();
    });
  };
}

function createCreateUserTable(connection: Connection) {
  return function createUserTable(callback: AsyncResultArrayCallback<undefined, Error>) {
    connection.query(`
CREATE TABLE \`User\` (
  \`ID\` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  \`Name\` VARCHAR(256) NOT NULL,
  \`Family\` VARCHAR(256) NOT NULL,
  \`Age\` int NOT NULL DEFAULT '18' CHECK (\`Age\` > 0 AND \`Age\` <= 120),
  \`RoleID\` int NOT NULL \`RoleID\` int NOT NULL CHECK (\`Age\` > 30 OR (\`Age\` <= 30 AND \`RoleID\` <= 2)),
  CONSTRAINT \`user_roleID_role_foreign\` FOREIGN KEY (\`RoleID\`) REFERENCES \`Role\` (\`ID\`) ON DELETE CASCADE ON UPDATE CASCADE,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
`, (error) => {
      if (error !== null) {
        return callback(error);
      }
      console.info('> "User" table created.');
      return callback();
    });
  };
}

function createAddMaximumUsersLimit(connection: Connection) {
  return function addMaximumUsersLimit(callback: AsyncResultArrayCallback<undefined, Error>) {
    connection.query(`
CREATE TRIGGER \`maximum_user_table_rows\` BEFORE INSERT ON \`User\` FOR EACH ROW
BEGIN
  SELECT COUNT(*) INTO @counter FROM \`User\`;
  IF @counter >= 20 THEN
    SIGNAL SQLSTATE VALUE '99999'
      SET MESSAGE_TEXT = 'Maximum records limit reached.';
  END IF;
END;`, (error) => {
      if (error !== null) {
        return callback(error);
      }
      console.info('> Maximum "User" table rows limit created.');
      return callback();
    });
  };
}

function createInsertRoles(connection: Connection) {
  return function insertRoles(callback: AsyncResultArrayCallback<undefined, Error>) {
    Async.each(
      ['Student', 'Lecturer', 'Admin'],
      (role, itemCallback) => {
        connection.query(
          `INSERT INTO Role (\`RoleName\`) VALUES ('${role}')`,
          (error) => {
            if (error !== null) {
              return itemCallback(error);
            }
            return itemCallback();
          },
        );
      }, (error) => {
        if (error !== null) {
          return callback(error);
        }
        console.info('> Random roles inserted.');
        return callback();
      },
    );
  };
}

function createInsertRandomUsers(connection: Connection) {
  return function insertRandomUsers(callback: AsyncResultArrayCallback<undefined, Error>) {
    const users = times(10, () => {
      const randomAge = faker.random.number({ min: 1, max: 119 });
      const user: User = {
        age: randomAge,
        name: faker.name.firstName(),
        family: faker.name.lastName(),
        roleID: randomAge <= 30
          ? faker.random.number({ min: 1, max: 2 })
          : faker.random.number({ min: 2, max: 3 }),
      };
      return user;
    });
    Async.each(
      users,
      (user, itemCallback) => {
        const query = `INSERT INTO User (\`Name\`, \`Family\`, \`Age\`, \`RoleID\`) VALUES ('${user.name}', '${user.family}', ${user.age}, ${user.roleID})`;
        // console.debug(query);
        connection.query(
          query,
          (error) => {
            if (error !== null) {
              return itemCallback(error);
            }
            return itemCallback();
          },
        );
      }, (error) => {
        if (error !== null) {
          return callback(error);
        }
        console.info('> Random users inserted.');
        return callback();
      },
    );
  };
}

function createPrintUsersInfo(connection: Connection) {
  interface RawQueryRowResult {
    Name: string;
    Family: string;
    RoleName: string;
  }
  return function printUsersInfo(callback: AsyncResultArrayCallback<undefined, Error>) {
    connection.query(`
SELECT \`User\`.\`Name\`, \`User\`.\`Family\`, \`Role\`.\`RoleName\`
FROM \`User\`
INNER JOIN \`Role\` ON \`User\`.\`RoleID\`=\`Role\`.\`ID\`;
`, (error, results) => {
      if (error !== null) {
        return callback(error);
      }
      console.log('> Users Info:');
      console.table((results as RawQueryRowResult[]));
      return callback();
    });
  };
}

function createCloseConnection(connection: Connection) {
  return function closeConnection(callback: AsyncResultArrayCallback<undefined, Error>) {
    connection.end((error) => {
      if (error !== null) {
        return callback(error);
      }
      console.info('> Database connection closed.');
      return callback();
    });
  };
}

const connection = mysql.createConnection({
  host: config.database.host,
  user: config.database.user,
  password: config.database.password,
  database: config.database.name,
  port: config.database.port,
});

function main() {
  Async.series(
    [
      createConnect(connection),
      createDropRoleTable(connection),
      createDropUserTable(connection),
      createCreateRoleTable(connection),
      createCreateUserTable(connection),
      createAddMaximumUsersLimit(connection),
      createInsertRoles(connection),
      createInsertRandomUsers(connection),
      createPrintUsersInfo(connection),
      createCloseConnection(connection),
    ],
    (error) => {
      if (error !== null) {
        return console.error(error);
      }
      return console.info('Succeeded.');
    },
  );
}

if (require.main === module) {
  main();
}
