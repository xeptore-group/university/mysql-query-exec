# mysql-query-exec

Naghibzadeh advanced databases class exercise project

## Usage

### Execution Video

You can see an example of the application execution at [here](https://asciinema.org/a/396988).

### Installation

1. Download and install latest version of [Node.js](https://nodejs.org/en/download/current/) depending on your operating system
2. Clone/download the source code and navigate to its directory

   1. Using `Git`:

      ```sh
      git clone git@gitlab.com:xeptore-group/university/mysql-query-exec.git
      ```

   2. Direct download:

      <https://gitlab.com/xeptore-group/university/mysql-query-exec/-/archive/main/mysql-query-exec-main.tar.gz>

3. Install dependencies

   ```sh
   npm install --global pnpm
   pnpm install
   ```

4. Set application configuration parameters
   Copy `env.example` into `.env` and set each configuration parameter.

5. Compile/build/transpile the source code

   ```sh
   pnpm run build
   ```

6. Run the application

   ```sh
   pnpm run start
   ```
