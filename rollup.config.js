import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import strip from '@rollup/plugin-strip';
import typescript from '@rollup/plugin-typescript';
import del from 'rollup-plugin-delete';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';


export default {
  input: 'src/main.ts',
  output: {
    file: './dist/main.js',
    format: 'cjs',
  },
  external: Object.keys(pkg.dependencies),
  plugins: [
    del({
      targets: './dist/',
    }),
    json(),
    nodeResolve({}),
    commonjs(),
    strip({
      functions: ['assert.*'],
    }),
    replace({
      'process.env.VERSION': process.env.npm_package_version,
      'process.env.DESCRIPTION': pkg.description,
      preventAssignment: false,
    }),
    terser({
      format: {
        comments: false,
      },
      compress: true,
    }),
    typescript({
      sourceMap: false,
    }),
  ],
};
